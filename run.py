#!/usr/bin/env python

import argparse, numpy, openravepy, time

from HerbRobot import HerbRobot
from SimpleRobot import SimpleRobot
from HerbEnvironment import HerbEnvironment
from SimpleEnvironment import SimpleEnvironment


from AStarPlanner import AStarPlanner
from DepthFirstPlanner import DepthFirstPlanner
from BreadthFirstPlanner import BreadthFirstPlanner
from HeuristicRRTPlanner import HeuristicRRTPlanner
from RRTPlanner import RRTPlanner
def main(robot, planning_env, planner,plannerName):

    raw_input('Press any key to begin planning')

    start_config = numpy.array(robot.GetCurrentConfiguration())
    print 'start_config', start_config
    if robot.name == 'herb':
        goal_config = numpy.array([ 4.6, -1.76, 0.00, 1.96, -1.15, 0.87, -1.43] )
    else:
        goal_config = numpy.array([3.0, 0.0])
    print "Planning.."
    start_time = time.time()
    plan = planner.Plan(start_config, goal_config)
    time_elapsed = time.time()-start_time

    # print "Plan :" ,plan
    print "Number of Nodes in Plan" , len(plan)
    print "Distance Travelled (in metres)", computeDistanceTravelled(planning_env,plan)
    print "Time Taken (in seconds) ", time_elapsed
    
    if plannerName == 'hrrt':
        print "Shortening Path... Timeout 5 seconds"    
        plan_short = planning_env.ShortenPath(plan)
        # print "Short Plan :" ,plan_short
        print "Number of nodes in Shortened plan " , len(plan_short)
        print "Distance Travelled (in meters)", computeDistanceTravelled(planning_env,plan_short)

        #TO Plot shortened Path
        if visualize and hasattr(planning_env, 'InitializePlot'):
            planning_env.InitializePlot(goal_config)

            for i in range(len(plan_short)-1):
                planning_env.PlotEdge(plan_short[i],plan_short[i+1]) 
        plan = plan_short

    raw_input('Press any key to execute trajectory')
    traj = robot.ConvertPlanToTrajectory(plan)
    robot.ExecuteTrajectory(traj)

def computeDistanceTravelled(planning_env,plan):
    distance = 0
    for i in range(len(plan)-1):
        distance += planning_env.ComputeDistance(plan[i],plan[i+1])
    return distance

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='script for testing planners')
    
    parser.add_argument('-r', '--robot', type=str, default='simple',
                        help='The robot to load (herb or simple)')
    parser.add_argument('-p', '--planner', type=str, default='astar',
                        help='The planner to run (astar, bfs, dfs or hrrt)')
    parser.add_argument('-v', '--visualize', action='store_true',
                        help='Enable visualization of tree growth (only applicable for simple robot)')
    parser.add_argument('--resolution', type=float, default=0.1,
                        help='Set the resolution of the grid (default: 0.1)')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug logging')
    parser.add_argument('-m', '--manip', type=str, default='right',
                        help='The manipulator to plan with (right or left) - only applicable if robot is of type herb')
    args = parser.parse_args()
    
    openravepy.RaveInitialize(True, level=openravepy.DebugLevel.Info)
    openravepy.misc.InitOpenRAVELogging()

    if args.debug:
        openravepy.RaveSetDebugLevel(openravepy.DebugLevel.Debug)

    env = openravepy.Environment()
    env.SetViewer('qtcoin')
    env.GetViewer().SetName('Homework 2 Viewer')

    # First setup the environment and the robot
    visualize = args.visualize
    if args.robot == 'herb':
        robot = HerbRobot(env, args.manip)
        planning_env = HerbEnvironment(robot, args.resolution)
        visualize = False
    elif args.robot == 'simple':
        robot = SimpleRobot(env)
        planning_env = SimpleEnvironment(robot, args.resolution)
    else:
        print 'Unknown robot option: %s' % args.robot
        exit(0)

    # Next setup the planner
    if args.planner == 'astar':
        planner = AStarPlanner(planning_env, visualize)
        plannerName = 'astar'
    elif args.planner == 'rrt':
        planner = RRTPlanner(planning_env, visualize=visualize)
        plannerName = 'rrt'
    elif args.planner == 'bfs':
        planner = BreadthFirstPlanner(planning_env, visualize)
        plannerName = 'bfs'
    elif args.planner == 'dfs':
        planner = DepthFirstPlanner(planning_env, visualize)
        plannerName = 'dfs'
    elif args.planner == 'hrrt':
        planner = HeuristicRRTPlanner(planning_env, visualize)
        plannerName = 'hrrt'
    else:
        print 'Unknown planner option: %s' % args.planner
        exit(0)

    main(robot, planning_env, planner,plannerName)

    import IPython
    IPython.embed()

        
    
