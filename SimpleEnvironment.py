import numpy
import pylab as pl
from DiscreteEnvironment import DiscreteEnvironment
import IPython
import random
import time

MAX_DISTANCE = 0.5

class SimpleEnvironment(object):
    
    def __init__(self, herb, resolution):
        self.robot = herb.robot
        self.robot_name = self.robot.GetName()
        self.lower_limits = [-5., -5.]
        self.upper_limits = [5., 5.]
        self.boundary_limits = [[-5., -5.], [5., 5.]]
        self.discrete_env = DiscreteEnvironment(resolution, self.lower_limits, self.upper_limits)
        self.resolution = resolution
        # add an obstacle
        table = self.robot.GetEnv().ReadKinBodyXMLFile('models/objects/table.kinbody.xml')
        self.robot.GetEnv().Add(table)
        self.env = self.robot.GetEnv()

        table_pose = numpy.array([[ 0, 0, -1, 1.5], [-1, 0,  0, 0], [ 0, 1,  0, 0], [ 0, 0,  0, 1]])
        table.SetTransform(table_pose)

    def GetSuccessors(self, node_id):

        successors = []

        # TODO: Here you will implement a function that looks
        #  up the configuration associated with the particular node_id
        #  and return a list of node_ids that represent the neighboring
        #  nodes

        #Finding the number of nodes in each line
        NodesPerLine = [0,0]
        NodesPerLine[0] = (self.upper_limits[0] - self.lower_limits[0])/self.resolution
        NodesPerLine[1] = (self.upper_limits[1] - self.lower_limits[1])/self.resolution

        #print 'NodesPerLine', NodesPerLine

        #Finding the configuration corresponding to the node id
        config = self.discrete_env.NodeIdToConfiguration(node_id)

        #print 'config', config

        #Finding the coordinates corresponding to the node id
        coord = self.discrete_env.NodeIdToGridCoord(node_id)

        #print 'coord', coord
        #print 'resolution', self.resolution
        
        #Initialize the values of the neighbours
        Left_Nbour = None
        Right_Nbour = None
        Up_Nbour = None
        Down_Nbour = None

        #Finding the left neighbour if the 1st coordinate is greater than the lower grid 
        #coordinate limit (0)
        if (coord[0]>0):
            newConfig = [config[0]-self.resolution, config[1]]
            if not self.isColliding(newConfig):
                Left_Nbour = self.discrete_env.ConfigurationToNodeId(newConfig)
            else:
                Left_Nbour = None
        

        #Finding the right neighbour if the 1st coordinate is lesser than the number of 
        #nodes per line in that dimension
        if (coord[0]<NodesPerLine[0]-1):
            newConfig = [config[0]+self.resolution, config[1]]
            if not self.isColliding(newConfig):
                Right_Nbour = self.discrete_env.ConfigurationToNodeId(newConfig)
            else:
                Right_Nbour = None
            

        #Finding the top neighbour if the 2nd coordinate is greater than the lower grid 
        #coordinate limit (0)
        if (coord[1]>0):
            newConfig = [config[0], config[1]-self.resolution]
            if not self.isColliding(newConfig):
                Up_Nbour = self.discrete_env.ConfigurationToNodeId(newConfig)
            else: 
                Up_Nbour = None
            

        #Finding the bottom neighbour if the 2nd coordinate is lesser than the number of 
        #nodes per line in that dimension
        if (coord[1]<NodesPerLine[1]-1):
            newConfig = [config[0], config[1]+self.resolution]
            if not self.isColliding(newConfig):
                Down_Nbour = self.discrete_env.ConfigurationToNodeId(newConfig)
            else:
                Down_Nbour = None
            

        #Successors in order Left; Right; Up; Down
        # successors = [Left_Nbour, Right_Nbour, Up_Nbour, Down_Nbour]
        successors.append(Up_Nbour)
        successors.append(Down_Nbour)
        successors.append(Right_Nbour)
        successors.append(Left_Nbour)
        successors = [x for x in successors if x != None]
        return successors

    def ComputeDistance(self, start_id, end_id, costFn ='E'):

        dist = 0

        if type(start_id) is not int:
            costFn       = 'E'
            config_start = start_id
            config_end   = end_id
        else:
            config_start = self.discrete_env.NodeIdToConfiguration(start_id)
            config_end = self.discrete_env.NodeIdToConfiguration(end_id)

        #Manhattan distance
        if costFn == 'M':
            dist = abs(config_start[0] - config_end[0]) + abs(config_start[1] - config_end[1])

        #Euclidean distance
        elif costFn == 'E':
            dist = ((config_start[0] - config_end[0])**2 + (config_start[1] - config_end[1])**2)**0.5

        return dist

    def ComputeHeuristicCost(self, start_id, goal_id):

        # TODO: Here you will implement a function that 
        # computes the heuristic cost between the configurations
        # given by the two node ids
        return self.ComputeDistance(start_id, goal_id, 'M')

    def isColliding(self,config):
        init_pos = self.robot.GetTransform() #Copy current transfom to reset robot
        temp_pos = init_pos.copy() #Copy Current Transform -- NUMPY COPY
       
        temp_pos[0][3] = config[0] #Set X Position
        temp_pos[1][3] = config[1] #Set Y Position
           
        with self.env: #Lock Environment before changing robot pose
            self.robot.SetTransform(temp_pos)
            is_colliding =  self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
            self.robot.SetTransform(init_pos)
        return is_colliding

    def SetGoalParameters(self, goal_config, p = 0.2):
        self.goal_config = goal_config
        self.p = p
        
    def GenerateRandomConfiguration(self):
        rand_p = numpy.random.ranf()
        if (0 < rand_p < self.p):
            return self.goal_config
        elif (self.p < rand_p < 1):
            config = [0] * 2
            lower_limits, upper_limits = self.boundary_limits
            
            #Generate Random Numbers
            x_pos = random.uniform(lower_limits[0],upper_limits[0])
            y_pos = random.uniform(lower_limits[1],upper_limits[1])

            is_colliding = self.isColliding([x_pos,y_pos])
            
            #Generate new positions until it is collision free
            while is_colliding:
                x_pos = random.uniform(lower_limits[0],upper_limits[0])
                y_pos = random.uniform(lower_limits[1],upper_limits[1])

                is_colliding =  self.isColliding([x_pos,y_pos])
            
            config = [x_pos,y_pos]        
            return numpy.array(config)

    
    def Extend(self, start_config, end_config, limit_d= True):
        sample = 100
        x_interp = numpy.zeros(sample)
        y_interp = numpy.zeros(sample)

        x_start, x_end = start_config[0], end_config[0]
        y_start, y_end = start_config[1], end_config[1]

        x_interp = numpy.linspace(x_start,x_end,num = sample)
        y_interp = numpy.linspace(y_start,y_end,num = sample)
                
        for i in range(len(x_interp)): #Iterate through all the points
                    
            #Check for Collision
            is_colliding =  self.isColliding([x_interp[i],y_interp[i]])

            if is_colliding:
                #TODO : DEBUG GOAL TO NEAREST
                # return is_colliding,numpy.array([x_interp[i-1],y_interp[i-1]]) #Return Previous point
                return True,None
            
            if limit_d == True:
                curr_distance = self.ComputeDistance(start_config,numpy.array([x_interp[i],y_interp[i]]))
                if (curr_distance >= MAX_DISTANCE):
                    return False,numpy.array([x_interp[i-1],y_interp[i-1]])
           
        return False,end_config
    
    def ShortenPath(self, path, timeout=5.0):
    
        to_exit  = time.time() + timeout
        while (time.time() < to_exit):
            #Choose two random vertices 
            vertex_1,vertex_2 = sorted(random.sample(range(len(path)),2))

            #If consecutive indices skip
            if vertex_1+1 == vertex_2:  
                continue
            
            #Check for collision
            is_colliding = self.Extend(path[vertex_1],path[vertex_2],limit_d = False)[0]
            
            #If no collision remove vertices inbetween two points
            if is_colliding == False:
                for i in range(vertex_1+1,vertex_2):
                    path[i] = None
                path = [x for x in path if x != None]
        return path

    def InitializePlot(self, goal_config):
        self.fig = pl.figure()
        pl.xlim([self.lower_limits[0], self.upper_limits[0]])
        pl.ylim([self.lower_limits[1], self.upper_limits[1]])
        pl.plot(goal_config[0], goal_config[1], 'gx')

        # Show all obstacles in environment
        for b in self.robot.GetEnv().GetBodies():
            if b.GetName() == self.robot.GetName():
                continue
            bb = b.ComputeAABB()
            pl.plot([bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0]],
                    [bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1]], 'r')
                    
                     
        pl.ion()
        pl.show()
        
    def PlotEdge(self, sconfig, econfig):
        pl.plot([sconfig[0], econfig[0]],
                [sconfig[1], econfig[1]],
                'g.-', linewidth=2.5)
        pl.draw()

