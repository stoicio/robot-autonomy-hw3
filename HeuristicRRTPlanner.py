import numpy
from RRTTree import RRTTree


class treeNodes:
    def __init__(self,vid):
        self.nodeID = vid

class HeuristicRRTPlanner(object):

    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize
        
         #Intialize plot only for pr2
        if (self.planning_env.robot_name == "pr2") and self.visualize:
            self.to_plot = True
        else:
            self.to_plot = False

    def Plan(self, start_config, goal_config, epsilon = 0.001):

        def getNodeQuality(vertexID,vertexConfig):
            cost = getCost(vertexID,vertexConfig)
            nodeQuality = 1 - ((cost - tree.optimalCost)/(tree.maxCost-tree.optimalCost))
            return nodeQuality

        def getCost(vertexID,vertexConfig):
            costToCome = 0
            prevID = vertexID
            currVertex = vertexConfig
            while not(currVertex == start_config).all():
                nextID = tree.edges[prevID]
                currVertex = tree.vertices[nextID]
                costToCome += self.planning_env.ComputeHeuristicCost(tree.vertices[prevID],currVertex)
                prevID = nextID
            costToGo = self.planning_env.ComputeHeuristicCost(vertexConfig,goal_config)
            return costToCome + costToGo
        
        tree = RRTTree(self.planning_env, start_config,goal_config)
        plan = []

        if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
            self.planning_env.InitializePlot(goal_config)
        
        #Set Goal Parameters with default P
        self.planning_env.SetGoalParameters(goal_config,) #Format to change P
        
        #Initialize Nearest with start_config
        nearest = start_config.copy() 
        extend  = nearest.copy()
        #Intialize Threshold Distance to check
        thresh_distance = self.planning_env.ComputeDistance(nearest,goal_config)

        while not (extend == goal_config).all():
            p_extend = extend.copy()
            #Get Next Vertex  to expand to
            target            = self.planning_env.GenerateRandomConfiguration()
            #Find the visited vertex closest to the new target
            nearestID,nearest = tree.GetNearestVertex(target)
            
            nodeQuality = getNodeQuality(nearestID,nearest)

            p = max(nodeQuality,tree.pMin)
            rand_p = numpy.random.ranf()
            
            if rand_p > p:
                continue

            #Get the point closest to the target without collision
            extend            = self.planning_env.Extend(nearest,target,True)[1]

            #If there is a feasible path
            if extend is not None:
                newID = tree.AddVertex(extend) #Add vertex to tree and get its ID
                tree.AddEdge(nearestID,newID) #Join two vertices
                thresh_distance = self.planning_env.ComputeDistance(nearest,goal_config) #Compute the new threshold distance
                tree.maxCost= max(getCost(newID,extend),tree.maxCost)
                if self.to_plot:
                    self.planning_env.PlotEdge(tree.vertices[nearestID],tree.vertices[newID]) 
            else:
                #Skip this vertex if not feasible path is found
                extend = p_extend.copy()
                continue
            

        #BACKTRACKING TO FIND PATH--------------------------------------
        # tree.edges structure --> { Vertex ID : Parent/Start Vertex ID}
        #Get the ID for the goal node
        prev_ID = tree.edges.keys()[-1]
        #Get Vertex at the ID
        curr_vertex = tree.vertices[prev_ID]
        #Append Vertex to the plan
        plan.append(tree.vertices[prev_ID])

        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)


        while not (curr_vertex == start_config).all():
            next_ID     = tree.edges[prev_ID] #Get Next ID
            curr_vertex = tree.vertices[next_ID]  #Get Vertex at ID
            if self.to_plot:
                self.planning_env.PlotEdge(tree.vertices[prev_ID],curr_vertex)  
            plan.append(curr_vertex)
            prev_ID = next_ID

        #Reverse Path
        plan = plan[::-1]
        return plan

        