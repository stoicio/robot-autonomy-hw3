import numpy
import IPython
import math
from openravepy import databases, IkParameterization
class DiscreteEnvironment(object):

    def __init__(self, resolution, lower_limits, upper_limits,option1 = None):

        # Store the resolution
        self.resolution = resolution

        # Store the bounds
        self.lower_limits = lower_limits
        self.upper_limits = upper_limits

        # Calculate the dimension
        self.dimension = len(self.lower_limits)

        # Figure out the number of grid cells that are in each dimension
        self.num_cells = self.dimension*[0]
        self.dim_limits = self.dimension*[0]
        temp_dim_limits = 1
        for idx in range(self.dimension):
            self.num_cells[idx] = numpy.ceil((upper_limits[idx] - lower_limits[idx])/resolution)
            self.dim_limits[idx] = self.num_cells[idx]*temp_dim_limits
            temp_dim_limits = self.dim_limits[idx]
        self.dim_limits.insert(0,1)    


    def ConfigurationToGridCoord(self, config):
        
        # TODO:
        # This function maps a configuration in the full configuration space
        # to a grid coordinate in discrete space
        
        coord = [0] * self.dimension

        #Subtract lower limit in calculation as grid always starts from zero
        for i in range(0,self.dimension):
            coord[i] = math.floor(round(((config[i]-self.lower_limits[i])/self.resolution),5))
        return coord

    def GridCoordToConfiguration(self, coord):
        
        # TODO:
        # This function smaps a grid coordinate in discrete space
        # to a configuration in the full configuration space
        
        config = [0] * self.dimension
        #Add lower limit as configuration begins from this point.
        #Add half the resolution to get the configuration value in the centre of the grid square
        for i in range(0,self.dimension):
            config[i] = round(((coord[i]*self.resolution)+(self.resolution/2.0)+self.lower_limits[i]),5)

        return config

    def GridCoordToNodeId(self,coord):
        
        # TODO:
        # This function maps a grid coordinate to the associated
        # node id 

        node_id=0
        for i in range(0,self.dimension):
            node_id+=coord[i]*self.dim_limits[i]

        return int(node_id)

    def NodeIdToGridCoord(self, node_id):
        
        # TODO:
        # This function maps a node id to the associated
        # grid coordinate

        coord = [0] * self.dimension
        for i in reversed(range (0,self.dimension)):
            coord[i] = math.floor(node_id/self.dim_limits[i])
            node_id = node_id%self.dim_limits[i]

        return coord
    

    def ConfigurationToNodeId(self, config):
        
        # TODO:
        # This function maps a node configuration in full configuration
        # space to a node in discrete space

        node_id = 0

        #Using Functions
        coord = self.ConfigurationToGridCoord(config)
        node_id = self.GridCoordToNodeId(coord)

        return node_id

    def NodeIdToConfiguration(self, nid):
        
        # TODO:
        # This function maps a node in discrete space to a configuraiton
        # in the full configuration space

        config = [0] * self.dimension

        #Using Functions
        coord = self.NodeIdToGridCoord(nid)
        config = self.GridCoordToConfiguration(coord)

        return config
        
