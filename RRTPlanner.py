import numpy
from RRTTree import RRTTree

class RRTPlanner(object):

    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize

        #Intialize plot only for pr2
        if (self.planning_env.robot_name == "pr2"):
            self.to_plot = True
        else:
            self.to_plot = False
        

    def Plan(self, start_config, goal_config, epsilon = 0.001):
        
        tree = RRTTree(self.planning_env, start_config,goal_config)
        plan = []
        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)
            
        #PSEUDOCODE :
            # 1.Set Goal Parameters
            # 2.While ( dist(nearest and goal > epsilon))
            #     a. target   = GenerateRandomConfiguration
            #     b. nearest  = ChooseNearestVertex
            #     c. extended = Extend(nearest,target)
            #     d. if extended not None
            #             i.  add Extended to Tree
            # 3.Backtrack through edges to find relevant path
            # 4.Append vertices to plan
            # 3.Reverse & Return plan
        
        #Set Goal Parameters with default P
        self.planning_env.SetGoalParameters(goal_config,) #Format to change P
        
        #Initialize Nearest with start_config
        nearest = start_config.copy() 
        extend  = nearest.copy()
        #Intialize Threshold Distance to check
        thresh_distance = self.planning_env.ComputeDistance(nearest,goal_config)

        # while thresh_distance > epsilon:
        while not (extend == goal_config).all():
            p_extend = extend.copy()
            #Get Next Vertex  to expand to
            target            = self.planning_env.GenerateRandomConfiguration()
            #Find the visited vertex closest to the new target
            nearestID,nearest = tree.GetNearestVertex(target)
            #Get the point closest to the target without collision
            extend            = self.planning_env.Extend(nearest,target)[1]

            #If there is a feasible path
            if extend is not None:
                newID = tree.AddVertex(extend) #Add vertex to tree and get its ID
                tree.AddEdge(nearestID,newID) #Join two vertices
                thresh_distance = self.planning_env.ComputeDistance(nearest,goal_config) #Compute the new threshold distance
                if self.to_plot:
                    self.planning_env.PlotEdge(tree.vertices[nearestID],tree.vertices[newID]) 
            else:
                #Skip this vertex if not feasible path is found
                extend = p_extend.copy()
                continue
            

        #BACKTRACKING TO FIND PATH--------------------------------------
        # tree.edges structure --> { Vertex ID : Parent/Start Vertex ID}
        #Get the ID for the goal node
        # prev_ID = tree.edges[len(tree.edges)]
        prev_ID = tree.edges.keys()[-1]
        #Get Vertex at the ID
        curr_vertex = tree.vertices[prev_ID]
        #Append Vertex to the plan
        plan.append(tree.vertices[prev_ID])

        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)


        while not (curr_vertex == start_config).all():
            next_ID     = tree.edges[prev_ID] #Get Next ID
            curr_vertex = tree.vertices[next_ID]  #Get Vertex at ID
            if self.to_plot:
                self.planning_env.PlotEdge(tree.vertices[prev_ID],curr_vertex)  
            plan.append(curr_vertex)
            prev_ID = next_ID

        #Reverse Path
        plan = plan[::-1]
        return plan






