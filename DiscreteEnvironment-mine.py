import numpy
import IPython
from openravepy import databases, IkParameterization
class DiscreteEnvironment(object):

    def __init__(self, resolution, lower_limits, upper_limits,option1 = None):

        # Store the resolution
        self.resolution = resolution

        # Store the bounds
        self.lower_limits = lower_limits
        self.upper_limits = upper_limits

        # Calculate the dimension
        self.dimension = len(self.lower_limits)

        # Figure out the number of grid cells that are in each dimension
        self.num_cells = self.dimension*[0]
        for idx in range(self.dimension):
            self.num_cells[idx] = numpy.ceil((upper_limits[idx] - lower_limits[idx])/resolution)


    def ConfigurationToGridCoord(self, config):
        
        # TODO:
        # This function maps a configuration in the full configuration space
        # to a grid coordinate in discrete space
        
        coord = [0] * self.dimension

        #Subtract lower limit in calculation as grid always starts from zero
        for i in range(0,self.dimension):
            coord[i] = int(round((config[i]-self.lower_limits[i]-0.01)/self.resolution))
            # coord[i] = int(round((config[i]-self.lower_limits[i])/self.resolution))
            #Subtracting 0.01 cuz 0.5 rounds to 1 instead of 0
        return coord

    def GridCoordToConfiguration(self, coord):
        
        # TODO:
        # This function smaps a grid coordinate in discrete space
        # to a configuration in the full configuration space
        
        config = [0] * self.dimension
        #Add lower limit as configuration begins from this point.
        #Add half the resolution to get the configuration value in the centre of the grid square
        for i in range(0,self.dimension):
            config[i] = ((coord[i]*self.resolution)+(self.resolution/2.0)+self.lower_limits[i])

        return config

    def GridCoordToNodeId(self,coord):
        
        # TODO:
        # This function maps a grid coordinate to the associated
        # node id 

        # node_id = 0
        # if self.dimension==2:
        #     for i in range(0,coord[1]):
        #         for i in range(0, int(self.num_cells[0])):
        #             node_id+=1
        #     node_id+=coord[0]

        # else:
        node_id = 0
        temp = 1
        for i in reversed(range(0,self.dimension)):
            for j in reversed(range(0,i)):
                temp *= int (self.num_cells[j])
                #print 'no of cells', self.num_cells[j]+1
            temp*= coord[i]
            #print 'temp', temp
            node_id += temp
            temp = 1

        return node_id

    def NodeIdToGridCoord(self, node_id):
        
        # TODO:
        # This function maps a node id to the associated
        # grid coordinate

        coord = [0] * self.dimension
        if self.dimension==2:
            coord[0]=int(node_id%self.num_cells[0])
            coord[1]=int(node_id/self.num_cells[0])
        
        else:
            for i in reversed(range (2,self.dimension)):
                temp = 1
                for j in reversed(range (0,i)):
                    temp *= int (self.num_cells[j])
                #print 'temp', temp
                coord[i] = int(node_id/temp)
                node_id = node_id - (coord[i]*temp)
                #print 'node_id', node_id

            coord[1]=int(node_id/self.num_cells[0])
            coord[0]=int(node_id%self.num_cells[0])

        return coord
    

    def ConfigurationToNodeId(self, config):
        
        # TODO:
        # This function maps a node configuration in full configuration
        # space to a node in discrete space

        node_id = 0

        #Using Functions
        coord = self.ConfigurationToGridCoord(config)
        node_id = self.GridCoordToNodeId(coord)

        #TODO If using functions is not permitted - redo code
        return node_id

    def NodeIdToConfiguration(self, nid):
        
        # TODO:
        # This function maps a node in discrete space to a configuraiton
        # in the full configuration space

        config = [0] * self.dimension

        #Using Functions
        coord = self.NodeIdToGridCoord(nid)
        config = self.GridCoordToConfiguration(coord)
        
        #TODO If using functions is not permitted - redo code
        return config
        
