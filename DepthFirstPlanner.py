import numpy as np
import pylab as pl 

class SearchNode:
    nodeID = 0
    parentNode = None
    config = []

    def __init__(self,nodeID,planning_env=None):
        self.nodeID = nodeID
        if planning_env is not None:
            self.config = planning_env.discrete_env.NodeIdToConfiguration(nodeID)
    def __eq__(self,other):
        return self.nodeID == other.nodeID

    def getPathToStart(self,plan,planning_env,plot = True):
        tempNode = self
        while tempNode.parentNode:
            plan.append(tempNode.config)
            tempNode = tempNode.parentNode
        plan.append(tempNode.config)
        plan = plan[::-1]
        if plot:
            for i in range(len(plan)-1):
                planning_env.PlotEdge(plan[i],plan[i+1])
        return plan

class DepthFirstPlanner(object):
    
    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize
        self.nodes = []

        #Intialize plot only for pr2
        if (self.planning_env.robot_name == "pr2") and self.visualize:
            self.to_plot = True

        else:
            self.to_plot = False

    def Plan(self, start_config, goal_config):
        
        plan = []
        
        # TODO: Here you will implement the depth first planner
        #  The return path should be a numpy array
        #  of dimension k x n where k is the number of waypoints
        #  and n is the dimension of the robots configuration space

        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)

        nodeStack = []
        current_node = SearchNode(self.planning_env.discrete_env.ConfigurationToNodeId(start_config))
        current_node.config = start_config
        current_node.parentNode = None
        nodeStack.append(current_node)
        goal_node = self.planning_env.discrete_env.ConfigurationToNodeId(goal_config)
        #Neighbours generated Left Right Up Down
        while(current_node.nodeID!= goal_node):
            current_node = nodeStack.pop(0)
            
            if (current_node.nodeID!=None) and (current_node not in self.nodes):
                if self.to_plot:
                    vertex = self.planning_env.discrete_env.NodeIdToConfiguration(current_node.nodeID)
                    pl.plot(vertex[0],vertex[1],'r.')
                    pl.draw()
                successors = self.planning_env.GetSuccessors(current_node.nodeID)
                successor_Nodes = []
                for ID in successors:
                    if ID is not None:
                        tempNode = SearchNode(ID,self.planning_env)
                        tempNode.parentNode = current_node
                        successor_Nodes.append(tempNode)

                nodeStack = successor_Nodes + nodeStack
                self.nodes.append(current_node)

        plan = current_node.getPathToStart(plan,self.planning_env,plot = self.to_plot)

        return plan
