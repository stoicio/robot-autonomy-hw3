import heapq
import copy
import pylab as pl
import IPython

class SearchNode():
    nodeID       = 0
    # parentNodeID = None
    parentNode   = None
    gCost        = 0 #Cost From Start
    hCost        = 0 #Cost To Goal
    totalCost    = 0
    config       = []

    def __init__(self,nodeID,planning_env=None):
        self.nodeID = nodeID
        if planning_env is not None:
            self.config = planning_env.discrete_env.NodeIdToConfiguration(nodeID)

    def setCostToGoal(self,hCost):
        self.hCost = hCost
    

    def setCostFromStart(self,gCost):
        if self.parentNode:
            self.gCost = self.parentNode.gCost+gCost
        else:
            self.gCost = gCost

    def setTotalCost(self):
        self.totalCost = self.gCost + self.hCost

    def getPathToStart(self,plan,planning_env,plot = True):
        tempNode = self
        while tempNode.parentNode:
            plan.append(tempNode.config)
            tempNode = tempNode.parentNode
        plan.append(tempNode.config)
        plan = plan[::-1]
        if plot:
            for i in range(len(plan)-1):
                planning_env.PlotEdge(plan[i],plan[i+1])
        return plan 

    def __eq__(self,other):
        return self.nodeID == other.nodeID
    def __cmp__(self,other):
        return cmp(self.totalCost,other.totalCost) 
    def __hash__(self):
        return hash(self.totalCost)
    

def check_state_visited(q, CurrentNode):
    Visited = 0
    for node in q:
            if(node==CurrentNode):
                    Visited = 1;
    return Visited

class AStarPlanner(object):
    
     
    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize
        self.nodes = dict()

        #Intialize plot only for pr2
        if (self.planning_env.robot_name == "pr2") and self.visualize:
            self.to_plot = True
        else:
            self.to_plot = False

    def Plan(self, start_config, goal_config):
        plan = []

        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)

        pathCost = 0

        goalNode = SearchNode(
                self.planning_env.discrete_env.ConfigurationToNodeId(
                goal_config))

        
        startNode = SearchNode(
                self.planning_env.discrete_env.ConfigurationToNodeId(
                start_config))

        goalNode.config  = goal_config
        startNode.config = start_config
        
        visitedNodes = []
        openList = []

        currNode = copy.deepcopy(startNode)

        while True:
            successors = self.planning_env.GetSuccessors(currNode.nodeID)
            for ID in successors:

                tempNode = SearchNode(ID,self.planning_env)

                # tempNode.setCostFromStart(
                #     currNode.gCost + self.planning_env.resolution)
                
                tempNode.setCostFromStart(
                    self.planning_env.ComputeDistance(ID,currNode.nodeID))

                tempNode.setCostToGoal(
                    self.planning_env.ComputeHeuristicCost(
                        ID,goalNode.nodeID))
                
                tempNode.setTotalCost()
                
                tempNode.parentNode = currNode
                
                if check_state_visited(visitedNodes,tempNode)==0:
                    heapq.heappush(openList, tempNode)
                    visitedNodes.append(tempNode)
            
            currNode = copy.deepcopy(heapq.heappop(openList))
            if self.to_plot:
                vertex = self.planning_env.discrete_env.NodeIdToConfiguration(currNode.nodeID)
                pl.plot(vertex[0],vertex[1],'b.')
                pl.draw()
            
            # if self.planning_env.ComputeDistance(currNode.nodeID, goalNode.nodeID)<BREAK_THRESH:
            if currNode == goalNode:
                plan = currNode.getPathToStart(plan,self.planning_env,plot = self.to_plot)
                break
        print 'Visited Nodes Count', len(visitedNodes)
        return plan
